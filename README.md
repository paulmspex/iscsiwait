## README ##

This is a little OpenRC service script called *iscsiwait*. 

If you are using an iSCSI storage target, the *iscsid* service will exit before the disk device entries are created in `/dev`. If you put lines in `/etc/fstab` for those disks with the `_netdev` option, then the *netmount* service may try to mount them before the device entries are created and will thus fail. 

So, this additional service will check whether the targets have corresponding disk device entries in /dev by calling `iscsiadm` with the right parameters and comparing the number of configured targets against the number of targets that have an associated device entry. If they are equal then the service exits. It will retry ten times at one second intervals before failing. 

There is also a file for the *netmount* service that goes into `/etc/conf.d`. It changes the service configuration so that the *netmount* service will wait for the *iscsiwait* service to finish before starting up. That way *netmount* won't try to mount iSCSI disks until *iscsiwait* succeeds. 

### Warnings and Notes ###
* Developed and tested on Alpine 3.12.1
* **Not** tested at all with multipath
